import pickle


class XShelve:
    def __init__(self, path):
        self.shelve_path = path
        try:
            with open(self.shelve_path, 'rb') as shelvefile:
                try:
                    self.cache = pickle.load(shelvefile)
                except EOFError:
                    self.cache = {}
        except IOError:
            self.cache = {}

    def __setitem__(self, key, value):
        self.cache[key] = value

    def sync(self):
        with open(self.shelve_path, 'wb') as shelvefile:
            pickle.dump(self.cache, shelvefile)

    def get(self, key):
        return self.cache.get(key)

    def keys(self):
        return self.cache.keys()

    def items(self):
        return self.cache.items()


def xopen(path):
    return XShelve(path)